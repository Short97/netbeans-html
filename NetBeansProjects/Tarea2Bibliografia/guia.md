### Configuracion inicial de git (solo hacer una vez despues de instalar)
```shell
git config --global user.name "Short97"
git config --global user.email "adrian_short97@hotmail.com"
```

### Obtener codigo del repositorio de git
```shell
git pull
```

### Subir codigo al repositorio de git
```shell
git add . # Agregar todos los archivos
git commit -m "Escribir aqui que es lo que estas subiendo" # Hacer commit 
git push # Enviarlos al repositorio remoto
```

### Clonar un repositorio remoto
```shell
git clone https://gitlab.com/Short97/netbeans-html.git
```

